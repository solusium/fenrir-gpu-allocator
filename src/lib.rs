use {
    abi_stable::{
        export_root_module,
        prefix_type::PrefixTypeTrait,
        sabi_extern_fn,
        sabi_trait::{TD_CanDowncast, TD_Opaque},
        std_types::{
            RArc, RBox,
            ROption::{self, RNone, RSome},
            RResult::{self, RErr, ROk},
            Tuple2,
        },
        DynTrait,
    },
    alloc::sync::Arc,
    ash::vk::MemoryRequirements,
    async_ffi::{BorrowingFfiFuture, FutureExt},
    async_lock::Mutex,
    bytemuck::bytes_of,
    core::mem::take,
    fenrir_error::{initialization_failed, Error},
    fenrir_hal::{
        allocation::{Metadata, Type},
        allocator::{
            manager::Inner,
            stable_abi::{Module, ModuleRef},
        },
    },
    gpu_allocator::{
        vulkan::{AllocationCreateDesc, AllocationScheme, AllocatorCreateDesc},
        AllocationError, AllocatorDebugSettings, MemoryLocation,
    },
    sleipnir::ffi::runtime::Handle,
};

extern crate alloc;

#[derive(Debug, Eq, PartialEq)]
enum Memory {
    Ash(fenrir_ash_interface::device::Memory),
}

#[derive(Debug)]
struct Allocation {
    memory: Memory,
    allocation: Option<Box<gpu_allocator::vulkan::Allocation>>,
    allocator: RArc<Allocator>,
    runtime: Handle,
}

#[derive(Clone, Debug)]
struct Allocator {
    allocator: Arc<Mutex<gpu_allocator::vulkan::Allocator>>,
    runtime: Handle,
    device: fenrir_hal::device::guard::Guard,
}

#[derive(Debug)]
struct Manager {
    runtime: Handle,
}

#[inline]
#[export_root_module]
fn instantiate_root_module() -> ModuleRef {
    Module { new, allocator }.leak_into_prefix()
}

#[inline]
#[sabi_extern_fn]
#[allow(improper_ctypes_definitions)]
fn new(runtime: Handle) -> DynTrait<'static, RArc<()>, Inner> {
    DynTrait::from_ptr(RArc::new(Manager { runtime }))
}

#[inline]
#[sabi_extern_fn]
#[allow(improper_ctypes_definitions)]
fn allocator(
    state: DynTrait<'static, RArc<()>, Inner>,
    device: fenrir_hal::device::guard::Guard,
) -> RResult<fenrir_hal::allocator::stable_abi::Trait_TO<RArc<()>>, Error> {
    let ash_device_handle: fenrir_ash_interface::device::Handle = match device.handle().try_into() {
        Ok(ash_device_handle_) => ash_device_handle_,
        Err(_) => unreachable!(),
    };

    let allocator_result = gpu_allocator::vulkan::Allocator::new(&AllocatorCreateDesc {
        instance: (**ash_device_handle.instance()).clone(),
        device: (*ash_device_handle).clone(),
        physical_device: ash_device_handle.physical_device(),
        debug_settings: AllocatorDebugSettings::default(),
        buffer_device_address: false,
        allocation_sizes: Default::default(),
    });

    match allocator_result {
        Err(error) => RErr(into_error(error)),
        Ok(gpu_allocator) => {
            let allocator = Arc::new(Mutex::new(gpu_allocator));

            match state.downcast_as::<Manager>() {
                Err(error) => RErr(initialization_failed(&format!(
                    "failed to cast {state:?} to a fenrir_gpu_allocator::Manager: {error}",
                ))),
                Ok(manager) => {
                    let runtime = manager.runtime.clone();

                    ROk(fenrir_hal::allocator::stable_abi::Trait_TO::from_ptr(
                        RArc::new(Allocator {
                            allocator,
                            runtime,
                            device,
                        }),
                        TD_CanDowncast,
                    ))
                }
            }
        }
    }
}

impl fenrir_hal::allocator::stable_abi::Trait for Allocator {
    fn allocate<'a>(
        &self,
        metadata: Metadata<'a>,
        this: fenrir_hal::allocator::stable_abi::Trait_TO<RArc<()>>,
    ) -> BorrowingFfiFuture<
        'a,
        RResult<fenrir_hal::allocation::stable_abi::Trait_TO<RBox<()>>, Error>,
    > {
        let runtime = self.runtime.clone();

        let gpu_allocator = self.allocator.clone();

        async move {
            let size = metadata.requirements.size as u64;
            let alignment = metadata.requirements.alignment as u64;
            let memory_type_bits = metadata.requirements.flags as u32;

            let location = match metadata.allocation_type {
                Type::GpuOnly => MemoryLocation::GpuOnly,
                Type::CpuToGpu => MemoryLocation::CpuToGpu,
                Type::GpuToCpu => MemoryLocation::GpuToCpu,
            };

            let allocation_result = gpu_allocator.lock().await.allocate(&AllocationCreateDesc {
                name: metadata.name.as_ref(),
                requirements: MemoryRequirements {
                    size,
                    alignment,
                    memory_type_bits,
                },
                location,
                linear: true,
                allocation_scheme: AllocationScheme::GpuAllocatorManaged,
            });

            match allocation_result {
                Err(error) => RErr(into_error(error)),
                Ok(gpu_allocation) => {
                    let boxed_allocation = Box::new(gpu_allocation);

                    let memory = Memory::Ash(fenrir_ash_interface::device::Memory::new(unsafe {
                        boxed_allocation.memory()
                    }));

                    let allocation = Some(boxed_allocation);

                    match this.clone().obj.downcast_into::<Allocator>() {
                        Err(error) => RErr(initialization_failed(&format!(
                            "failed to cast {this:?} to a fenrir_gpu_allocator::Allocator: {error}",
                        ))),
                        Ok(allocator) => {
                            ROk(fenrir_hal::allocation::stable_abi::Trait_TO::from_value(
                                Allocation {
                                    memory,
                                    allocation,
                                    allocator,
                                    runtime,
                                },
                                TD_Opaque,
                            ))
                        }
                    }
                }
            }
        }
        .into_ffi()
    }
}

impl Eq for Allocator {}

impl PartialEq for Allocator {
    #[inline]
    fn eq(&self, other: &Self) -> bool {
        self.device == other.device
    }
}

impl Drop for Allocation {
    #[inline]
    fn drop(&mut self) {
        let allocator = self.allocator.clone();

        let maybe_allocation = take(&mut self.allocation);

        let _join_handle = self.runtime.spawn(async move {
            match maybe_allocation {
                None => unreachable!(),
                Some(allocation) => {
                    if allocator.allocator.lock().await.free(*allocation).is_err() {
                        unreachable!();
                    }
                }
            }
        });
    }
}

impl Eq for Allocation {}

impl PartialEq for Allocation {
    fn eq(&self, other: &Self) -> bool {
        let allocations = (self.allocation.as_ref(), other.allocation.as_ref());

        if let (Some(left_allocation), Some(right_allocation)) = allocations {
            let values = [
                self.memory == other.memory,
                left_allocation.chunk_id() == right_allocation.chunk_id(),
                unsafe { left_allocation.memory() == right_allocation.memory() },
                left_allocation.is_dedicated() == right_allocation.is_dedicated(),
                left_allocation.offset() == right_allocation.offset(),
                left_allocation.size() == right_allocation.size(),
                left_allocation.mapped_ptr() == right_allocation.mapped_ptr(),
                left_allocation.mapped_slice() == right_allocation.mapped_slice(),
                left_allocation.is_null() == right_allocation.is_null(),
            ];

            values.iter().all(|value| *value)
        } else {
            unreachable!()
        }
    }
}

impl fenrir_hal::allocation::stable_abi::Trait for Allocation {
    fn as_mut_ptr(&mut self) -> ROption<*mut u8> {
        match self.allocation.as_ref() {
            None => unreachable!(),
            Some(allocation) => match allocation.mapped_ptr().map(|ptr| ptr.as_ptr() as *mut u8) {
                Some(pointer) => RSome(pointer),
                None => RNone,
            },
        }
    }

    fn as_ptr(&self) -> ROption<*const u8> {
        match self.allocation.as_ref() {
            None => unreachable!(),
            Some(allocation) => {
                match allocation.mapped_ptr().map(|ptr| ptr.as_ptr() as *const u8) {
                    Some(pointer) => RSome(pointer),
                    None => RNone,
                }
            }
        }
    }

    fn handle(&self) -> Tuple2<*const u8, usize> {
        let bytes = match &self.memory {
            Memory::Ash(memory) => bytes_of(memory),
        };

        (bytes.as_ptr(), bytes.len()).into()
    }

    fn offset(&self) -> usize {
        match self.allocation.as_ref() {
            Some(allocation) => allocation.offset() as usize,
            None => unreachable!(),
        }
    }

    fn size(&self) -> usize {
        match self.allocation.as_ref() {
            Some(allocation) => allocation.size() as usize,
            None => unreachable!(),
        }
    }
}

#[cold]
#[inline]
fn into_error(error: AllocationError) -> Error {
    match error {
        AllocationError::OutOfMemory => Error::OutOfDeviceMemory,
        AllocationError::FailedToMap(_) => Error::MemoryMapFailed,
        error => Error::StateNotRecoverable(format!("unexpected error: {error}").into()),
    }
}

unsafe impl Send for Allocator {}
unsafe impl Sync for Allocator {}
