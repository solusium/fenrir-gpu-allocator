# fenrir-ash

The fenrir-gpu-allocator crate is a plugin and allocator implementation for the
[fenrir crate](https://gitlab.com/solusium/fenrir) based on
[gpu-allocator](https://github.com/Traverse-Research/gpu-allocator).
